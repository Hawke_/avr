/*
 * main.c
 *
 *  Created on: Jul 26, 2017
 *      Author: nathan
 *
 *      To Test the functionality of EEPROM
 *      Fill the eeprom with random words of data, like we will in the data logger
 *      Read the eeprom sequentially and print out the string representation of the values
 */

#include <avr/io.h>
#include <avr/eeprom.h>
#include <util/delay.h>
#include <stdlib.h>
#include <SoftSerialOut.h>



#define EEPROM_SIZE	512
#define RAND_MAX 0xFFFF

int main(void) {
	sout_init();

	while(1) {
		for(int i=0; i < EEPROM_SIZE/16; i++) {
			eeprom_busy_wait();
			eeprom_write_word((i*16), rand());
		}
		for(uint16_t i=0; i<EEPROM_SIZE/16; i++) {
			char ival[2];
			sprintf(ival, "%d", i);
			sout_str(ival);
			sout_str(" - ");
			uint16_t word = eeprom_read_word((i*16));
			sout_str((char) word & 0xFF);
			sout_str((char) word >> 8);
			sout_str("\n");
			_delay_ms(500);
		}
		sout_str("End of eeprom read\n");
	}


	return 0;
}
