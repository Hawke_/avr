/*
 * SoftSerialOut.h
 *
 *  Created on: Jul 10, 2017
 *      Author: Nathan Frana
 */

#ifndef SOFTSERIALOUT_H_
#define SOFTSERIALOUT_H_

#include <avr/io.h>

void sout_init(void);

void sout_chr(uint8_t chr);

void sout_str(char *s);

#endif /* SOFTSERIALOUT_H_ */
