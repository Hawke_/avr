/*
 * SoftSerialOut.c
 *
 *  Created on: Jul 10, 2017
 *      Author: Nathan Frana
 *
 *      Software serial out on pin PB0 @ 1200 baud
 *      System clock at @ 8MHz
 *      Uses the 8bit Timer0
 *
 */

#include "SoftSerialOut.h"
#include <avr/io.h>
#include <avr/interrupt.h>

//Baud-1200
//Prescaler-256
//26 ticks per baud

#define TICKS	26

void sout_init(void) {
/* Set output on PB0 to HIGH*/
	TCCR0B = (1<<CS02) | (1 << CS01);
	DDRB = (DDRB | (1 << DDB0));
}

void sout_chr(uint8_t chr) {
/* Disable Interrupts */
	cli();

/* Start bit - TX to LOW */
	PORTB = (0 << PB0);
	TCNT0 = 0;
	while (TCNT0 < TICKS){

	}

/* Transmit character */
	for(int i = 0; i < 8; i++) { 	//Char
		PORTB = (((chr >> i) & 1) << PB0);
		TCNT0 = 0;
		while (TCNT0 < TICKS){

		}
	}

/* End bit - TX to HIGH */
	PORTB = (1 << PB0);
	TCNT0 = 0;
	while (TCNT0 < TICKS){

	}

/* Enable Interrupts */
	sei();
}

void sout_str(char *s) {
	while(*s) {
		sout_chr(*s++);
	}
}
