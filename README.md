# README #

This is a repo to contain my adventures into 8-bit AVRs 
 
### Projects ###
##RPM Signal Emulator
Using an Attiny84 and a NPN & PNP high side transistor switch made it possible to generate a 33% duty cycle 12V square wave,
emulating the output a MSD7AL-2 would send to a tachometer. This was used to test an autometer tachometer for accuracy. .

##Software Serial Out
A library that allows for output only serial communications from an avr to a computer for debugging, 
in my case an arduino with serial passthrough. It is able to run on any avr (with modifications) 
only using a single pin and 8-bit timer, freeing up UART hardware for production level communications. 

##Data Logger
Data logger to be used on a pulling truck and incorperate sensors like RPM, oil pressure, etc..

#EEPROMTest
Test the EEPROM functions and learn eeprom functionality.
#LinMasterTest & LinSlaveTest
Test LIN functions and learn functionality
	
