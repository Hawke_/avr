/*
 * main.c
 *
 *  Created on: Jun 26, 2017
 *      Author: Nathan Frana
 *      MSD 7AL-2 tachometer signal emulator using an Attiny84 and a high side transistor switch
 *
 */

#define F_CPU 8000000UL

#include <avr/io.h>


uint16_t rpm;
uint16_t frequency;
uint16_t totalTimerTicks;   //Total # of clock ticks per pulse
uint16_t pulseHighTicks;	//# ticks output is high
uint16_t pulseLowTicks;		//# ticks output is low

void setup(void){
	/* Setup Pin */
	DDRA = 0xFF;

	/* Setup Timer */
	TCCR1B = (0 << CS02) | (1 << CS01) | (0 << CS00);	//16-bit - 8 pre-scaler

	rpm = 500;
}

void increment(void){
	/*Increment rpm by 500, max 9000*/
	rpm = (rpm + 500) % 9500;
	frequency = 4*rpm / 60;
	totalTimerTicks = 15000000 / rpm;
	pulseHighTicks = totalTimerTicks * .33 ;
	pulseLowTicks = totalTimerTicks * .67;
}

void pulse(void) {
	/* Pulse on PA5 with a ~33% duty cycle */
	TCNT1 = 0;
	PORTA |= _BV(PA5);
	while(TCNT1 < pulseHighTicks) {/*Wait*/}

	TCNT1 = 0;
	PORTA &= ~_BV(PA5);
	while(TCNT1 < pulseLowTicks) {/*Wait*/}

}

int main(void){
	setup();

	/* Main Loop */
	while(1) {
		increment();
		int pulseCount = 2*frequency;				//# of pulses that will take ~2 seconds
		for(int i = 0; i <= pulseCount; i++) {
			pulse();
		}
	}
}




